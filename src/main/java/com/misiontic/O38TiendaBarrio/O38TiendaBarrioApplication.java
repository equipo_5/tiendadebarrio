package com.misiontic.O38TiendaBarrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O38TiendaBarrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(O38TiendaBarrioApplication.class, args);
	}

}
